﻿using RabbitMQ.Client.Events;
using System.Text;

var factory = new ConnectionFactory() { HostName = "localhost", ConsumerDispatchConcurrency = 2 };

using (var connection = factory.CreateConnection())
using (var channel = connection.CreateModel())
{
    // Declare the queue
    channel.QueueDeclare(queue: "your_queue_name", durable: false, exclusive: false, autoDelete: false, arguments: null);

    // Set the prefetch count to receive multiple messages at once
    channel.BasicQos(prefetchSize: 0, prefetchCount: 5, global: false);

    var consumer = new EventingBasicConsumer(channel);
    consumer.Received += (model, ea) =>
    {
        try
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine($"Received message: {message}");

            // Simulate some processing time
            System.Threading.Thread.Sleep(1000);

            // Acknowledge the message
            channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
        }
        catch (Exception ex)
        {
            // Handle exception or log error
            Console.WriteLine($"Error processing message: {ex.Message}");
            channel.BasicNack(ea.DeliveryTag, false, true);
        }
    };

    // Start consuming messages
    channel.BasicConsume(queue: "your_queue_name", autoAck: false, consumer: consumer);

    for (int i = 0; i < 10; i++)
    {
        channel.BasicPublish(string.Empty, "your_queue_name", null, Encoding.UTF8.GetBytes(i.ToString()));
    }

    Console.WriteLine("Press [Enter] to exit.");
    Console.ReadLine();
}