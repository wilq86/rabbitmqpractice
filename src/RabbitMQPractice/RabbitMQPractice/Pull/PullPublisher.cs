﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Direct
{
    public class PullPublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.pull", ExchangeType.Fanout, false, true, null);

            //An auto-delete queue that never had a consumer will not be deleted.
            Channel.QueueDeclare("q.pull", false, false, true, null);
            Channel.QueueBind("q.pull", "ex.pull", string.Empty);
        }

        public void Publish(string message)
        {
            Channel.BasicPublish("ex.pull", string.Empty, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
