﻿using RabbitMQ.Client.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice.Pull
{
    public class PullConsumer : RabbitMqConnection
    {
        public string QueueName;

        private bool _disposed = false;

        public PullConsumer(string queueName)
        {
            QueueName = queueName;
        }

        public override void Configure()
        {

        }

        public List<string> ReadAll()
        {
            List<string> messages = new List<string>();

            BasicGetResult result;
            while ((result = Channel.BasicGet(QueueName, true)) != null)
            {
                string message = Encoding.UTF8.GetString(result.Body.ToArray());
                messages.Add(message);
            }


            return messages;
        }

        protected override void Dispose(bool disposing)
        {

            if (!_disposed)
            {
                if (disposing)
                {
                    //An auto-delete queue that never had a consumer will not be deleted.
                    //BasicGet is not counted as consumer
                    //so we need to delete it manualy
                    Channel.QueueDelete(QueueName);
                }
            }

            _disposed = true;
        }
    }

}
