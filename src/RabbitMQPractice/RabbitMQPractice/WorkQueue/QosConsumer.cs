﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice
{
    public class QosConsumer : RabbitMqConnection, IDisposable 
    {
        public ConcurrentBag<string> Messages = new ConcurrentBag<string>();

        public string QueueName;
        private string ConsumerTag = string.Empty;

        private bool _disposed = false;

        public QosConsumer(string queueName)
        {
            QueueName = queueName;
        }

        public override void Configure()
        {
            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            ConsumerTag = Channel.BasicConsume(QueueName, false, consumer);
        }

        private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Body.ToArray());
            Thread.Sleep(int.Parse(message));

            Channel.BasicAck(e.DeliveryTag, false);
            Messages.Add(message);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.BasicCancel(ConsumerTag);
                }

                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}
