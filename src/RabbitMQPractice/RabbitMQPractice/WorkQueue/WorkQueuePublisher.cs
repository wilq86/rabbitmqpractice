﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Direct
{
    public class WorkQueuePublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.work_queue", ExchangeType.Direct, true, true, null);

            Channel.QueueDeclare("q.work_queue", true, false, true, null);
            Channel.QueueBind("q.work_queue", "ex.work_queue", "test");
        }

        public void Publish(string message)
        {
            Channel.BasicPublish("ex.work_queue", "test", null, Encoding.UTF8.GetBytes(message));
        }
    }
}
