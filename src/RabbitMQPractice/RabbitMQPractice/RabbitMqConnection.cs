﻿namespace RabbitMQPractice
{
    public abstract class RabbitMqConnection : IDisposable
    {
        protected ConnectionFactory ConnectionFactory = default!;
        protected IConnection Connection = default!;
        protected IModel Channel = default!;

        private bool _disposed = false;

        public RabbitMqConnection()
        {
            
        }

        public void Start()
        {
            ConnectionFactory = new ConnectionFactory()
            {
                HostName = "localhost",
                VirtualHost = "/",
                Port = 5672,
                UserName = "guest",
                Password = "guest",
            };

            Connection = ConnectionFactory.CreateConnection();
            Channel = Connection.CreateModel();

            Configure();
        }

        public abstract void Configure();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.Close();
                    Connection.Close();
                }

                _disposed = true;
            }
        }
    }
}
