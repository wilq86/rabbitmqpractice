﻿using RabbitMQ.Client.Events;
using RabbitMQPractice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice.RequestReply
{
    public class Requestor : RabbitMqConnection, IDisposable
    {
        public ConcurrentBag<string> Messages = new ConcurrentBag<string>();

        private string ConsumerTag = string.Empty;

        private bool _disposed = false;

        public override void Configure()
        {
            Channel.QueueDeclare("q.requestor", true, false, true);
            Channel.QueueDeclare("q.replier", true, false, true);

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            ConsumerTag = Channel.BasicConsume("q.replier", false, consumer);
        }

        public void Publish(string message)
        {
            Channel.BasicPublish(string.Empty, "q.requestor", null, Encoding.UTF8.GetBytes(message));
        }

        private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Body.ToArray());
            Channel.BasicAck(e.DeliveryTag, false);
            Messages.Add(message);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.BasicCancel(ConsumerTag);
                }

                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}



        

