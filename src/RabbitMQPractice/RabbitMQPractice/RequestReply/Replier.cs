﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice.RequestReply
{
    public class Replier : RabbitMqConnection, IDisposable
    {
        private string ConsumerTag = string.Empty;

        private bool _disposed = false;

        public override void Configure()
        {
            Channel.QueueDeclare("q.requestor", true, false, true);

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            ConsumerTag = Channel.BasicConsume("q.requestor", false, consumer);

        }

        private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Body.ToArray());
            Channel.BasicAck(e.DeliveryTag, false);

            Channel.BasicPublish(string.Empty, "q.replier", null, Encoding.UTF8.GetBytes($"Echo: {message}"));
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.BasicCancel(ConsumerTag);
                }

                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}
