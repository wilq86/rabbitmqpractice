﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.Header
{
    public class HeaderConsumer : RabbitMqConnection
    {
        public ConcurrentBag<string> Messages = new ConcurrentBag<string>();

        public string QueueName;

        public HeaderConsumer(string queueName)
        {
            QueueName = queueName;
        }

        public override void Configure()
        {
            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            var consumerTag = Channel.BasicConsume(QueueName, false, consumer);
        }

        private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Body.ToArray());
            Channel.BasicAck(e.DeliveryTag, false);

            Messages.Add(message);
        }
    }
}
