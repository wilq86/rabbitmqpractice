﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.Header
{
    public class HeaderPublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.headers", ExchangeType.Headers, true, true, null);

            Channel.QueueDeclare("q.queue_pdf", true, false, true, null);
            Channel.QueueBind("q.queue_pdf", "ex.headers", string.Empty,
                new Dictionary<string, object>() {
                    { "x-match", "all" },
                    { "format", "pdf" },
                }
            );

            Channel.QueueDeclare("q.queue_pdf_log", true, false, true, null);
            Channel.QueueBind("q.queue_pdf_log", "ex.headers", string.Empty,
                new Dictionary<string, object>() {
                    { "x-match", "all" },
                    { "format", "pdf" },
                    { "type", "log" },
                }
            );

            Channel.QueueDeclare("q.queue_log", true, false, true, null);
            Channel.QueueBind("q.queue_log", "ex.headers", string.Empty,
                new Dictionary<string, object>() {
                    { "x-match", "all" },
                    { "type", "log" },
                }
            );
        }

        public void Publish(string message, Dictionary<string, object> headers)
        {
            var properties = Channel.CreateBasicProperties();
            properties.Headers = headers;
            Channel.BasicPublish("ex.headers", string.Empty, properties, Encoding.UTF8.GetBytes(message));
        }
    }
}
