﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.Fanout
{
    public class FanoutPublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.fanout", ExchangeType.Fanout, true, true, null);

            Channel.QueueDeclare("q.fanout_1", true, false, true, null);
            Channel.QueueBind("q.fanout_1", "ex.fanout", string.Empty);

            Channel.QueueDeclare("q.fanout_2", true, false, true, null);
            Channel.QueueBind("q.fanout_2", "ex.fanout", string.Empty);
        }

        public void Publish(string message)
        {
            Channel.BasicPublish("ex.fanout", string.Empty, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
