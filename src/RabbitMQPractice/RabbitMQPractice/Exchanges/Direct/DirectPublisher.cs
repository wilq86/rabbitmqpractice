﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.Direct
{
    public class DirectPublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.direct", ExchangeType.Direct, true, true, null);

            Channel.QueueDeclare("q.queue_info", true, false, true, null);
            Channel.QueueBind("q.queue_info", "ex.direct", "info");

            Channel.QueueDeclare("q.queue_warn", true, false, true, null);
            Channel.QueueBind("q.queue_warn", "ex.direct", "warn");

            Channel.QueueDeclare("q.queue_error", true, false, true, null);
            Channel.QueueBind("q.queue_error", "ex.direct", "error");
        }

        public void Publish(string message, string routingKey)
        {
            Channel.BasicPublish("ex.direct", routingKey, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
