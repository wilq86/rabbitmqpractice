﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.Topic
{
    public class TopicPublisher : RabbitMqConnection
    {
        //class.level.message
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.topic", ExchangeType.Topic, true, true, null);

            Channel.QueueDeclare("q.topic_info_level", true, false, true, null);
            Channel.QueueBind("q.topic_info_level", "ex.topic", "*.info.*");

            Channel.QueueDeclare("q.topic_class_object", true, false, true, null);
            Channel.QueueBind("q.topic_class_object", "ex.topic", "object.*.*");

            Channel.QueueDeclare("q.topic_message_hello", true, false, true, null);
            Channel.QueueBind("q.topic_message_hello", "ex.topic", "#.hello");
        }

        public void Publish(string message, string routingKey)
        {
            Channel.BasicPublish("ex.topic", routingKey, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
