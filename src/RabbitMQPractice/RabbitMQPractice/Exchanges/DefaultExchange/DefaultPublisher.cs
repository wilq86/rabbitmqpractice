﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.DefaultExchange
{
    public class DefaultPublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.QueueDeclare("q.queue_1", true, false, true, null);
            Channel.QueueDeclare("q.queue_2", true, false, true, null);
            Channel.QueueDeclare("q.queue_3", true, false, true, null);
        }

        public void Publish(string message, string routingKey)
        {
            Channel.BasicPublish(string.Empty, routingKey, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
