﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.AlternateExchange
{
    public class AlternatePublisher : RabbitMqConnection
    {
        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.alternate_dead_letter_queue", ExchangeType.Fanout, true, true, null);

            Channel.ExchangeDeclare("ex.alternate_main", ExchangeType.Direct, true, true, new Dictionary<string, object>()
            {
                {"alternate-exchange" , "ex.alternate_dead_letter_queue" }
            });

            Channel.QueueDeclare("q.alternate_info", true, false, true, null);
            Channel.QueueBind("q.alternate_info", "ex.alternate_main", "info");

            Channel.QueueDeclare("q.alternate_warn", true, false, true, null);
            Channel.QueueBind("q.alternate_warn", "ex.alternate_main", "warn");

            Channel.QueueDeclare("q.alternate_error", true, false, true, null);
            Channel.QueueBind("q.alternate_error", "ex.alternate_main", "error");

            Channel.QueueDeclare("q.alternate_dead_letter_queue", true, false, true, null);
            Channel.QueueBind("q.alternate_dead_letter_queue", "ex.alternate_dead_letter_queue", string.Empty);
        }

        public void Publish(string message, string routingKey)
        {
            Channel.BasicPublish("ex.alternate_main", routingKey, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
