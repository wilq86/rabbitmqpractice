﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Exchanges.ExchangeToExchange
{
    public class EchangeToExchange : RabbitMqConnection
    {
        private bool _disposed = false;

        public override void Configure()
        {
            Channel.ExchangeDeclare("ex.exchange_1", ExchangeType.Direct, false, true, null);
            Channel.ExchangeDeclare("ex.exchange_2", ExchangeType.Direct, false, true, null);

            Channel.ExchangeBind("ex.exchange_1", "ex.exchange_2", "one");
            Channel.ExchangeBind("ex.exchange_2", "ex.exchange_1", "two");

            Channel.QueueDeclare("q.exchange_queue_1", true, false, true, null);
            Channel.QueueBind("q.exchange_queue_1", "ex.exchange_1", "one");

            Channel.QueueDeclare("q.exchange_queue_2", true, false, true, null);
            Channel.QueueBind("q.exchange_queue_2", "ex.exchange_2", "two");
        }

        public void Publish(string exchange, string routing_key, string message)
        {
            Channel.BasicPublish(exchange, routing_key, null, Encoding.UTF8.GetBytes(message));
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    //Exchange must be unbound to destroy them
                    Channel.ExchangeUnbind("ex.exchange_1", "ex.exchange_2", "one");
                    Channel.ExchangeUnbind("ex.exchange_2", "ex.exchange_1", "two");
                }

                _disposed = true;
            }
        }
    }
}
