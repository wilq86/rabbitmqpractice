﻿# To do:

1. Request/Reply with id
2. Publish with confirmation
> [tutorial](https://www.rabbitmq.com/tutorials/tutorial-seven-dotnet.html)
> [docs](https://www.rabbitmq.com/confirms.html)
3. Priority
4. CheckList
> [docs](https://www.rabbitmq.com/production-checklist.html)