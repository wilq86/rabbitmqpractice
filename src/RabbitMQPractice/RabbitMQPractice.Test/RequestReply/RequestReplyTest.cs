﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.RequestReply;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.RequestReply
{
    [TestFixture]
    public class RequestReplyTest
    {
        [Test]
        public void Test()
        {
            using Requestor requestor = new Requestor();
            requestor.Start();

            using Replier replier = new Replier();
            replier.Start();

            requestor.Publish("Hello");
            Thread.Sleep(10);

            requestor.Messages.Count.Should().Be(1);
            requestor.Messages.ElementAt(0).Should().Be("Echo: Hello");

            requestor.Publish("World");
            Thread.Sleep(10);

            requestor.Messages.Count.Should().Be(2);
            requestor.Messages.Contains("Echo: Hello");
            requestor.Messages.Contains("Echo: World");
        }
    }
}
