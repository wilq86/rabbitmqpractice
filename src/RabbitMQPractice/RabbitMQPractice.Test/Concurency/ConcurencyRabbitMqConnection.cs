﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Concurency
{
    public abstract class ConcurencyRabbitMqConnection : IDisposable
    {
        protected ConnectionFactory ConnectionFactory = default!;
        protected IConnection Connection = default!;
        protected IModel Channel = default!;
        protected ushort Consumers;

        private bool _disposed = false;

        public ConcurencyRabbitMqConnection(ushort consumers)
        {
            Consumers = consumers;
        }

        public void Start()
        {
            ConnectionFactory = new ConnectionFactory()
            {
                HostName = "localhost",
                VirtualHost = "/",
                Port = 5672,
                UserName = "guest",
                Password = "guest",
                ConsumerDispatchConcurrency = Consumers,
            };

            Connection = ConnectionFactory.CreateConnection();
            Channel = Connection.CreateModel();

            Configure();
        }

        public abstract void Configure();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.Close();
                    Connection.Close();
                }

                _disposed = true;
            }
        }
    }
}
