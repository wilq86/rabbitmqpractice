﻿using FluentAssertions;
using RabbitMQPractice.Test.Concurency;

namespace RabbitMQPractice.Test.WorkQueue
{
    [TestFixture]
    public class ConcurencyQueueTest
    {
        [Test]
        public void Concurency()
        {
            using ConcurentPublisher publisher = new ConcurentPublisher();
            publisher.Start();

            using ConcurencyConsumer consumer1 = new ConcurencyConsumer(10);
            consumer1.Start();

            for (int i = 0; i < 10; i++)
            {
                publisher.Publish("2000");
            }

            Thread.Sleep(1000);

            consumer1.Messages.Count.Should().Be(10);
        }
    }
}
