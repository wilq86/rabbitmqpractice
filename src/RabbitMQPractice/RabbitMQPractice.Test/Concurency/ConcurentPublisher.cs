﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Concurency
{
    public class ConcurentPublisher : RabbitMqConnection
    {
        public const string QueueName = "q.concurent_queue";

        public override void Configure()
        {
            Channel.QueueDeclare(QueueName, false, false, true, null);
        }

        public void Publish(string message)
        {
            Channel.BasicPublish(string.Empty, QueueName, null, Encoding.UTF8.GetBytes(message));
        }
    }
}
