﻿using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Concurency
{
    public class ConcurencyConsumer : ConcurencyRabbitMqConnection, IDisposable
    {
        public ConcurrentBag<string> Messages = new ConcurrentBag<string>();

        private bool _disposed = false;
        private string ConsumerTag = string.Empty;


        public ConcurencyConsumer( ushort consumers)
            : base(consumers)
        {
            Consumers = consumers;
        }

        public override void Configure()
        {
            Channel.BasicQos(0, Consumers, false);

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += Consumer_Received;

            ConsumerTag = Channel.BasicConsume(ConcurentPublisher.QueueName, false, consumer);
        }

        private void Consumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Body.ToArray());
            Messages.Add(message);

            Thread.Sleep(int.Parse(message));
            Channel.BasicAck(e.DeliveryTag, false);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Channel.BasicCancel(ConsumerTag);
                }

                _disposed = true;
            }
            base.Dispose(disposing);
        }
    }
}
