﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.WorkQueue
{
    [TestFixture]
    public class WorkQueueTest
    {
        [Test]
        public void Test()
        {
            using TestQueuePublisher publisher = new TestQueuePublisher();
            publisher.Start();

            using QosConsumer consumer1 = new QosConsumer("q.work_queue");
            consumer1.Start();

            using QosConsumer consumer2 = new QosConsumer("q.work_queue");
            consumer2.Start();

            using QosConsumer consumer3 = new QosConsumer("q.work_queue");
            consumer3.Start();

            publisher.Publish("200");
            publisher.Publish("100");
            publisher.Publish("1");

            publisher.Publish("1");
            publisher.Publish("1");

            publisher.Publish("1");

            Thread.Sleep(300);

            consumer1.Messages.Count.Should().Be(2);
            consumer2.Messages.Count.Should().Be(2);
            consumer3.Messages.Count.Should().Be(2);
        }
    }
}
