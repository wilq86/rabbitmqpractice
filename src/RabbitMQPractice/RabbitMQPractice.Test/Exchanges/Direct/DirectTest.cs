﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.Direct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Direct
{
    [TestFixture]
    public class DirectTest
    {
        [Test]
        public void Test()
        {
            using DirectPublisher publisher = new DirectPublisher();
            publisher.Start();

            using Consumer consumer_info = new Consumer("q.queue_info");
            consumer_info.Start();

            using Consumer consumer_warn = new Consumer("q.queue_warn");
            consumer_warn.Start();

            using Consumer consumer_error = new Consumer("q.queue_error");
            consumer_error.Start();

            publisher.Publish("1", "info");
            publisher.Publish("2", "info");
            publisher.Publish("3", "info");

            publisher.Publish("1", "warn");
            publisher.Publish("2", "warn");

            publisher.Publish("1", "error");

            Thread.Sleep(10);

            consumer_info.Messages.Count.Should().Be(3);
            consumer_warn.Messages.Count.Should().Be(2);
            consumer_error.Messages.Count.Should().Be(1);
        }
    }
}
