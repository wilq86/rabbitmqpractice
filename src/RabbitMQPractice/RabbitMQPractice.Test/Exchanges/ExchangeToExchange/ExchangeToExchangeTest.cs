﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.ExchangeToExchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.ExchangeToExchange
{
    [TestFixture]
    public class ExchangeToExchangeTest
    {
        [Test]
        public void Test()
        {
            using EchangeToExchange publisher = new EchangeToExchange();
            publisher.Start();

            using Consumer consumer1 = new Consumer("q.exchange_queue_1");
            consumer1.Start();

            using Consumer consumer2 = new Consumer("q.exchange_queue_2");
            consumer2.Start();

            publisher.Publish("ex.exchange_1", "one", "A");
            publisher.Publish("ex.exchange_1", "two", "B");

            publisher.Publish("ex.exchange_2", "one", "C");
            publisher.Publish("ex.exchange_2", "two", "D");

            Thread.Sleep(10);

            consumer1.Messages.Count.Should().Be(2);
            consumer2.Messages.Count.Should().Be(2);
        }
    }
}
