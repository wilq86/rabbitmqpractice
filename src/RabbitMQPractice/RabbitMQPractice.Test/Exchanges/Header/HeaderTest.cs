﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Header
{
    [TestFixture]
    public class HeaderTest
    {

        [Test]
        public void Test()
        {
            using HeaderPublisher publisher = new HeaderPublisher();
            publisher.Start();

            using Consumer consumer_pdf = new Consumer("q.queue_pdf");
            consumer_pdf.Start();

            using Consumer consumer_pdf_log = new Consumer("q.queue_pdf_log");
            consumer_pdf_log.Start();

            using Consumer consumer_log = new Consumer("q.queue_log");
            consumer_log.Start();

            publisher.Publish("1", new Dictionary<string, object> { { "format", "pdf" }, { "type", "log" } });
            publisher.Publish("2", new Dictionary<string, object> { { "format", "csv" }, { "type", "infomration" } });
            publisher.Publish("3", new Dictionary<string, object> { { "format", "pdf" } });

            Thread.Sleep(10);

            consumer_pdf.Messages.Count.Should().Be(2);
            consumer_pdf_log.Messages.Count.Should().Be(1);
            consumer_log.Messages.Count.Should().Be(1);
        }
    }
}
