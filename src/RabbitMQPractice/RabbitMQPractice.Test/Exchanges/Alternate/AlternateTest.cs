﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.AlternateExchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Alternate
{
    [TestFixture]
    public class AlternateTest
    {
        [Test]
        public void Test()
        {
            using AlternatePublisher publisher = new AlternatePublisher();
            publisher.Start();

            using Consumer consumer_info = new Consumer("q.alternate_info");
            consumer_info.Start();

            using Consumer consumer_warn = new Consumer("q.alternate_warn");
            consumer_warn.Start();

            using Consumer consumer_error = new Consumer("q.alternate_error");
            consumer_error.Start();

            using Consumer consumer_not_recognized = new Consumer("q.alternate_dead_letter_queue");
            consumer_not_recognized.Start();

            publisher.Publish("A", "info");
            publisher.Publish("B", "info");
            publisher.Publish("C", "info");

            publisher.Publish("D", "warn");
            publisher.Publish("E", "warn");

            publisher.Publish("F", "error");

            publisher.Publish("G", "er");
            publisher.Publish("H", "er");

            Thread.Sleep(10);

            consumer_info.Messages.Count.Should().Be(3);
            consumer_warn.Messages.Count.Should().Be(2);
            consumer_error.Messages.Count.Should().Be(1);

            consumer_not_recognized.Messages.Count.Should().Be(2);
        }
    }
}
