﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Topic
{
    [TestFixture]
    public class TopicTest
    {

        [Test]
        public void Test()
        {
            using TopicPublisher publisher = new TopicPublisher();
            publisher.Start();

            using Consumer consumer_info = new Consumer("q.topic_info_level");
            consumer_info.Start();

            using Consumer consumer_class = new Consumer("q.topic_class_object");
            consumer_class.Start();

            using Consumer consumer_message = new Consumer("q.topic_message_hello");
            consumer_message.Start();

            publisher.Publish("1", "object.info.hello");
            publisher.Publish("2", "string.warn.world");
            publisher.Publish("3", "int.error.hello");

            Thread.Sleep(10);

            consumer_info.Messages.Count.Should().Be(1);
            consumer_class.Messages.Count.Should().Be(1);
            consumer_message.Messages.Count.Should().Be(2);
        }
    }
}
