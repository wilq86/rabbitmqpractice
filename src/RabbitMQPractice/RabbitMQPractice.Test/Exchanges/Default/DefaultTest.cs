﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.DefaultExchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Default
{
    [TestFixture]
    public class DefaultTest
    {
        [Test]
        public void Test()
        {
            using DefaultPublisher publisher = new DefaultPublisher();
            publisher.Start();

            using Consumer consumer_info = new Consumer("q.queue_1");
            consumer_info.Start();

            using Consumer consumer_warn = new Consumer("q.queue_2");
            consumer_warn.Start();

            using Consumer consumer_error = new Consumer("q.queue_3");
            consumer_error.Start();

            publisher.Publish("A", "q.queue_1");
            publisher.Publish("B", "q.queue_1");
            publisher.Publish("C", "q.queue_1");

            publisher.Publish("D", "q.queue_2");
            publisher.Publish("E", "q.queue_2");

            publisher.Publish("F", "q.queue_3");

            Thread.Sleep(10);

            consumer_info.Messages.Count.Should().Be(3);
            consumer_warn.Messages.Count.Should().Be(2);
            consumer_error.Messages.Count.Should().Be(1);
        }
    }
}
