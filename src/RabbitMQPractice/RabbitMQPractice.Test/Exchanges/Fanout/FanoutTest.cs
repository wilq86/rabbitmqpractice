﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Exchanges.Fanout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Exchanges.Fanout
{
    [TestFixture]
    public class FanouTest
    {

        [Test]
        public void Test()
        {
            using FanoutPublisher publisher = new FanoutPublisher();
            publisher.Start();

            using Consumer consumer1 = new Consumer("q.fanout_1");
            consumer1.Start();

            using Consumer consumer2 = new Consumer("q.fanout_2");
            consumer2.Start();

            publisher.Publish("1");
            publisher.Publish("2");
            publisher.Publish("3");

            Thread.Sleep(10);

            consumer1.Messages.Count.Should().Be(3);
            consumer2.Messages.Count.Should().Be(3);
        }
    }
}
