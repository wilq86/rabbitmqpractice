﻿using FluentAssertions;
using RabbitMQPractice.Direct;
using RabbitMQPractice.Pull;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPractice.Test.Pull
{
    [TestFixture]
    public class PullTest
    {

        [Test]
        public void Test()
        {
            using PullPublisher publisher = new PullPublisher();
            publisher.Start();

            using PullConsumer consumer1 = new PullConsumer("q.pull");
            consumer1.Start();


            publisher.Publish("1");
            publisher.Publish("2");
            publisher.Publish("3");

            Thread.Sleep(10);

            consumer1.ReadAll().Count.Should().Be(3);
            consumer1.ReadAll().Count.Should().Be(0);
        }
    }
}
